package com.fomaster.vgamepad;

import com.fomaster.vgamepad.view.SplashScreen;
import javafx.application.Application;
import javafx.stage.Stage;

/**
 * Created by felip_000 on 09-01-2017.
 */
public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        new SplashScreen(primaryStage).show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
