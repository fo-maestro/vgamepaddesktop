/*
 * GamePadConstants.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */

package com.fomaster.vgamepad.control;

/**
 * KeyConstants for the android app
 *
 * @author Felipe Oyarzun
 * @version 1.0
 */
public final class GamePadConstants {

    private GamePadConstants() {}

    private static final int MAX_BIT = 0x3F;
    private static final int BUTTON_MASK = (1 << 2) | 1;
    private static final int STATE_MASK = ~(1 << BUTTON_MASK) & MAX_BIT;

    public static final int D_PAD_LEFT = 0x01;
    public static final int D_PAD_RIGHT = 0x02;
    public static final int D_PAD_UP = 0x03;
    public static final int D_PAD_DOWN = 0x04;

    public static final int BUTTON_PAD_X = 0x05;
    public static final int BUTTON_PAD_B = 0X06;
    public static final int BUTTON_PAD_Y = 0x07;
    public static final int BUTTON_PAD_A = 0x08;

    public static final int BUTTON_L1 = 0x09;
    public static final int BUTTON_L2 = 0x0A;
    public static final int BUTTON_R1 = 0x0B;
    public static final int BUTTON_R2 = 0x0C;

    public static final int BUTTON_START = 0x0D;
    public static final int BUTTON_SELECT = 0x0E;

    /**
     * Returns true if the key is pressed
     * @param keyCode The KeyCode
     * @return true if the key is pressed
     */
    public static boolean isKeyPressed(int keyCode) {
        return (keyCode >> BUTTON_MASK) == 1;
    }

    /**
     * Returns the Key without the BitState(MAX_BIT + 1)
     * @param keyCode The KeyCode
     * @return The key without the BitState
     */
    public static int getKeyWithOutState(int keyCode) {
        return keyCode & STATE_MASK;
    }
}
