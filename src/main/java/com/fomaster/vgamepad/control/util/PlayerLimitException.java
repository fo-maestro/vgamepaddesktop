/*
 * PlayerLimitException.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */

package com.fomaster.vgamepad.control.util;

/**
 * Thrown when the player number is higher than 4.
 *
 * @author Felipe Oyarzun
 * @version 1.0
 */
public class PlayerLimitException extends RuntimeException {
    public PlayerLimitException() {
        super();
    }

    public PlayerLimitException(String message) {
        super(message);
    }

    public PlayerLimitException(String message, Throwable cause) {
        super(message, cause);
    }

    public PlayerLimitException(Throwable cause) {
        super(cause);
    }
}
