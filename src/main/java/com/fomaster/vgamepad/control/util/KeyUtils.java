/*
 * KeyUtils.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */

package com.fomaster.vgamepad.control.util;

import javafx.scene.input.KeyCode;

import java.awt.event.KeyEvent;
import java.util.HashMap;
import java.util.Map;

/**
 * Contains all KeyMaps(AWT and FX) and Key transformers methods from AWT/FX to FX/AWT
 *
 * @author Felipe Oyarzun
 * @version 1.0
 */
public final class KeyUtils {

    private KeyUtils() {}

    private static Map<KeyCode, Integer> awtConstants = new HashMap<>();
    private static Map<Integer, KeyCode> fxConstants = new HashMap<>();

    static {
        //AWT Constants
        awtConstants.put(KeyCode.ACCEPT, KeyEvent.VK_ACCEPT);
        awtConstants.put(KeyCode.ADD, KeyEvent.VK_ADD);
        awtConstants.put(KeyCode.AGAIN, KeyEvent.VK_AGAIN);
        awtConstants.put(KeyCode.ALL_CANDIDATES, KeyEvent.VK_ALL_CANDIDATES);
        awtConstants.put(KeyCode.ALPHANUMERIC, KeyEvent.VK_ALPHANUMERIC);
        awtConstants.put(KeyCode.ALT, KeyEvent.VK_ALT);
        awtConstants.put(KeyCode.ALT_GRAPH, KeyEvent.VK_ALT_GRAPH);
        awtConstants.put(KeyCode.AMPERSAND, KeyEvent.VK_AMPERSAND);
        awtConstants.put(KeyCode.ASTERISK, KeyEvent.VK_ASTERISK);
        awtConstants.put(KeyCode.AT, KeyEvent.VK_AT);
        awtConstants.put(KeyCode.BACK_QUOTE, KeyEvent.VK_BACK_QUOTE);
        awtConstants.put(KeyCode.BACK_SLASH, KeyEvent.VK_BACK_SLASH);
        awtConstants.put(KeyCode.BACK_SPACE, KeyEvent.VK_BACK_SPACE);
        awtConstants.put(KeyCode.BEGIN, KeyEvent.VK_BEGIN);
        awtConstants.put(KeyCode.BRACELEFT, KeyEvent.VK_BRACERIGHT);
        awtConstants.put(KeyCode.CANCEL, KeyEvent.VK_CANCEL);
        awtConstants.put(KeyCode.CAPS, KeyEvent.VK_CAPS_LOCK);
        awtConstants.put(KeyCode.CIRCUMFLEX, KeyEvent.VK_CIRCUMFLEX);
        awtConstants.put(KeyCode.CLEAR, KeyEvent.VK_CLEAR);
        awtConstants.put(KeyCode.CLOSE_BRACKET, KeyEvent.VK_CLOSE_BRACKET);
        awtConstants.put(KeyCode.CODE_INPUT, KeyEvent.VK_CODE_INPUT);
        awtConstants.put(KeyCode.COLON, KeyEvent.VK_COLON);
        awtConstants.put(KeyCode.COMMA, KeyEvent.VK_COMMA);
        awtConstants.put(KeyCode.COMPOSE, KeyEvent.VK_COMPOSE);
        awtConstants.put(KeyCode.CONTEXT_MENU, KeyEvent.VK_CONTEXT_MENU);
        awtConstants.put(KeyCode.CONTROL, KeyEvent.VK_CONTROL);
        awtConstants.put(KeyCode.CONVERT, KeyEvent.VK_CONVERT);
        awtConstants.put(KeyCode.COPY, KeyEvent.VK_COPY);
        awtConstants.put(KeyCode.CUT, KeyEvent.VK_CUT);
        awtConstants.put(KeyCode.DEAD_ABOVEDOT, KeyEvent.VK_DEAD_ABOVEDOT);
        awtConstants.put(KeyCode.DEAD_ABOVERING, KeyEvent.VK_DEAD_ABOVERING);
        awtConstants.put(KeyCode.DEAD_ACUTE, KeyEvent.VK_DEAD_ACUTE);
        awtConstants.put(KeyCode.DEAD_BREVE, KeyEvent.VK_DEAD_BREVE);
        awtConstants.put(KeyCode.DEAD_CARON, KeyEvent.VK_DEAD_CARON);
        awtConstants.put(KeyCode.DEAD_CEDILLA, KeyEvent.VK_DEAD_CEDILLA);
        awtConstants.put(KeyCode.DEAD_CIRCUMFLEX, KeyEvent.VK_DEAD_CIRCUMFLEX);
        awtConstants.put(KeyCode.DEAD_DIAERESIS, KeyEvent.VK_DEAD_DIAERESIS);
        awtConstants.put(KeyCode.DEAD_DOUBLEACUTE, KeyEvent.VK_DEAD_DOUBLEACUTE);
        awtConstants.put(KeyCode.DEAD_GRAVE, KeyEvent.VK_DEAD_GRAVE);
        awtConstants.put(KeyCode.DEAD_IOTA, KeyEvent.VK_DEAD_IOTA);
        awtConstants.put(KeyCode.DEAD_MACRON, KeyEvent.VK_DEAD_MACRON);
        awtConstants.put(KeyCode.DEAD_OGONEK, KeyEvent.VK_DEAD_OGONEK);
        awtConstants.put(KeyCode.DEAD_SEMIVOICED_SOUND, KeyEvent.VK_DEAD_SEMIVOICED_SOUND);
        awtConstants.put(KeyCode.DEAD_TILDE, KeyEvent.VK_DEAD_TILDE);
        awtConstants.put(KeyCode.DEAD_VOICED_SOUND, KeyEvent.VK_DEAD_VOICED_SOUND);
        awtConstants.put(KeyCode.DECIMAL, KeyEvent.VK_DECIMAL);
        awtConstants.put(KeyCode.DELETE, KeyEvent.VK_DELETE);
        awtConstants.put(KeyCode.DOLLAR, KeyEvent.VK_DOLLAR);
        awtConstants.put(KeyCode.DOWN, KeyEvent.VK_DOWN);
        awtConstants.put(KeyCode.END, KeyEvent.VK_END);
        awtConstants.put(KeyCode.ENTER, KeyEvent.VK_ENTER);
        awtConstants.put(KeyCode.EQUALS, KeyEvent.VK_EQUALS);
        awtConstants.put(KeyCode.ESCAPE, KeyEvent.VK_ESCAPE);
        awtConstants.put(KeyCode.EURO_SIGN, KeyEvent.VK_EURO_SIGN);
        awtConstants.put(KeyCode.EXCLAMATION_MARK, KeyEvent.VK_EXCLAMATION_MARK);
        awtConstants.put(KeyCode.F1, KeyEvent.VK_F1);
        awtConstants.put(KeyCode.F2, KeyEvent.VK_F2);
        awtConstants.put(KeyCode.F3, KeyEvent.VK_F3);
        awtConstants.put(KeyCode.F4, KeyEvent.VK_F4);
        awtConstants.put(KeyCode.F5, KeyEvent.VK_F5);
        awtConstants.put(KeyCode.F6, KeyEvent.VK_F6);
        awtConstants.put(KeyCode.F7, KeyEvent.VK_F7);
        awtConstants.put(KeyCode.F8, KeyEvent.VK_F8);
        awtConstants.put(KeyCode.F9, KeyEvent.VK_F9);
        awtConstants.put(KeyCode.F10, KeyEvent.VK_F10);
        awtConstants.put(KeyCode.F11, KeyEvent.VK_F11);
        awtConstants.put(KeyCode.F12, KeyEvent.VK_F12);
        awtConstants.put(KeyCode.F13, KeyEvent.VK_F13);
        awtConstants.put(KeyCode.F14, KeyEvent.VK_F14);
        awtConstants.put(KeyCode.F15, KeyEvent.VK_F15);
        awtConstants.put(KeyCode.F16, KeyEvent.VK_F16);
        awtConstants.put(KeyCode.F17, KeyEvent.VK_F17);
        awtConstants.put(KeyCode.F18, KeyEvent.VK_F18);
        awtConstants.put(KeyCode.F19, KeyEvent.VK_F19);
        awtConstants.put(KeyCode.F20, KeyEvent.VK_F20);
        awtConstants.put(KeyCode.F21, KeyEvent.VK_F21);
        awtConstants.put(KeyCode.F22, KeyEvent.VK_F22);
        awtConstants.put(KeyCode.F23, KeyEvent.VK_F23);
        awtConstants.put(KeyCode.F24, KeyEvent.VK_F24);
        awtConstants.put(KeyCode.FINAL, KeyEvent.VK_FINAL);
        awtConstants.put(KeyCode.FIND, KeyEvent.VK_FIND);
        awtConstants.put(KeyCode.FULL_WIDTH, KeyEvent.VK_FULL_WIDTH);
        awtConstants.put(KeyCode.GREATER, KeyEvent.VK_GREATER);
        awtConstants.put(KeyCode.HALF_WIDTH, KeyEvent.VK_HALF_WIDTH);
        awtConstants.put(KeyCode.HELP, KeyEvent.VK_HELP);
        awtConstants.put(KeyCode.HIRAGANA, KeyEvent.VK_HIRAGANA);
        awtConstants.put(KeyCode.HOME, KeyEvent.VK_HOME);
        awtConstants.put(KeyCode.INPUT_METHOD_ON_OFF, KeyEvent.VK_INPUT_METHOD_ON_OFF);
        awtConstants.put(KeyCode.INSERT, KeyEvent.VK_INSERT);
        awtConstants.put(KeyCode.INVERTED_EXCLAMATION_MARK, KeyEvent.VK_INVERTED_EXCLAMATION_MARK);
        awtConstants.put(KeyCode.JAPANESE_HIRAGANA, KeyEvent.VK_JAPANESE_HIRAGANA);
        awtConstants.put(KeyCode.JAPANESE_KATAKANA, KeyEvent.VK_JAPANESE_KATAKANA);
        awtConstants.put(KeyCode.JAPANESE_ROMAN, KeyEvent.VK_JAPANESE_ROMAN);
        awtConstants.put(KeyCode.KANA, KeyEvent.VK_KANA);
        awtConstants.put(KeyCode.KANA_LOCK, KeyEvent.VK_KANA_LOCK);
        awtConstants.put(KeyCode.KANJI, KeyEvent.VK_KANJI);
        awtConstants.put(KeyCode.KATAKANA, KeyEvent.VK_KATAKANA);
        awtConstants.put(KeyCode.KP_DOWN, KeyEvent.VK_KP_DOWN);
        awtConstants.put(KeyCode.KP_LEFT, KeyEvent.VK_KP_LEFT);
        awtConstants.put(KeyCode.KP_RIGHT, KeyEvent.VK_KP_RIGHT);
        awtConstants.put(KeyCode.KP_UP, KeyEvent.VK_KP_UP);
        awtConstants.put(KeyCode.LEFT, KeyEvent.VK_LEFT);
        awtConstants.put(KeyCode.LEFT_PARENTHESIS, KeyEvent.VK_LEFT_PARENTHESIS);
        awtConstants.put(KeyCode.LESS, KeyEvent.VK_LESS);
        awtConstants.put(KeyCode.META, KeyEvent.VK_META);
        awtConstants.put(KeyCode.MINUS, KeyEvent.VK_MINUS);
        awtConstants.put(KeyCode.MODECHANGE, KeyEvent.VK_MODECHANGE);
        awtConstants.put(KeyCode.MULTIPLY, KeyEvent.VK_MULTIPLY);
        awtConstants.put(KeyCode.NONCONVERT, KeyEvent.VK_NONCONVERT);
        awtConstants.put(KeyCode.NUM_LOCK, KeyEvent.VK_NUM_LOCK);
        awtConstants.put(KeyCode.NUMBER_SIGN, KeyEvent.VK_NUMBER_SIGN);
        awtConstants.put(KeyCode.NUMPAD0, KeyEvent.VK_NUMPAD0);
        awtConstants.put(KeyCode.NUMPAD1, KeyEvent.VK_NUMPAD1);
        awtConstants.put(KeyCode.NUMPAD2, KeyEvent.VK_NUMPAD2);
        awtConstants.put(KeyCode.NUMPAD3, KeyEvent.VK_NUMPAD3);
        awtConstants.put(KeyCode.NUMPAD4, KeyEvent.VK_NUMPAD4);
        awtConstants.put(KeyCode.NUMPAD5, KeyEvent.VK_NUMPAD5);
        awtConstants.put(KeyCode.NUMPAD6, KeyEvent.VK_NUMPAD6);
        awtConstants.put(KeyCode.NUMPAD7, KeyEvent.VK_NUMPAD7);
        awtConstants.put(KeyCode.NUMPAD8, KeyEvent.VK_NUMPAD8);
        awtConstants.put(KeyCode.NUMPAD9, KeyEvent.VK_NUMPAD9);
        awtConstants.put(KeyCode.OPEN_BRACKET, KeyEvent.VK_OPEN_BRACKET);
        awtConstants.put(KeyCode.PAGE_DOWN, KeyEvent.VK_PAGE_DOWN);
        awtConstants.put(KeyCode.PAGE_UP, KeyEvent.VK_PAGE_UP);
        awtConstants.put(KeyCode.PASTE, KeyEvent.VK_PASTE);
        awtConstants.put(KeyCode.PAUSE, KeyEvent.VK_PAUSE);
        awtConstants.put(KeyCode.PERIOD, KeyEvent.VK_PERIOD);
        awtConstants.put(KeyCode.PLUS, KeyEvent.VK_PLUS);
        awtConstants.put(KeyCode.PREVIOUS_CANDIDATE, KeyEvent.VK_PREVIOUS_CANDIDATE);
        awtConstants.put(KeyCode.PRINTSCREEN, KeyEvent.VK_PRINTSCREEN);
        awtConstants.put(KeyCode.PROPS, KeyEvent.VK_PROPS);
        awtConstants.put(KeyCode.QUOTE, KeyEvent.VK_QUOTE);
        awtConstants.put(KeyCode.QUOTEDBL, KeyEvent.VK_QUOTEDBL);
        awtConstants.put(KeyCode.RIGHT, KeyEvent.VK_RIGHT);
        awtConstants.put(KeyCode.RIGHT_PARENTHESIS, KeyEvent.VK_RIGHT_PARENTHESIS);
        awtConstants.put(KeyCode.ROMAN_CHARACTERS, KeyEvent.VK_ROMAN_CHARACTERS);
        awtConstants.put(KeyCode.SCROLL_LOCK, KeyEvent.VK_SCROLL_LOCK);
        awtConstants.put(KeyCode.SEMICOLON, KeyEvent.VK_SEMICOLON);
        awtConstants.put(KeyCode.SEPARATOR, KeyEvent.VK_SEPARATOR);
        awtConstants.put(KeyCode.SHIFT, KeyEvent.VK_SHIFT);
        awtConstants.put(KeyCode.SLASH, KeyEvent.VK_SLASH);
        awtConstants.put(KeyCode.SPACE, KeyEvent.VK_SPACE);
        awtConstants.put(KeyCode.STOP, KeyEvent.VK_STOP);
        awtConstants.put(KeyCode.SUBTRACT, KeyEvent.VK_SUBTRACT);
        awtConstants.put(KeyCode.TAB, KeyEvent.VK_TAB);
        awtConstants.put(KeyCode.UNDEFINED, KeyEvent.VK_UNDEFINED);
        awtConstants.put(KeyCode.UNDERSCORE, KeyEvent.VK_UNDERSCORE);
        awtConstants.put(KeyCode.UNDO, KeyEvent.VK_UNDO);
        awtConstants.put(KeyCode.UP, KeyEvent.VK_UP);
        awtConstants.put(KeyCode.WINDOWS, KeyEvent.VK_WINDOWS);

        //FX Constants
        fxConstants.put(KeyEvent.VK_ACCEPT, KeyCode.ACCEPT);
        fxConstants.put(KeyEvent.VK_ADD, KeyCode.ADD);
        fxConstants.put(KeyEvent.VK_AGAIN, KeyCode.AGAIN);
        fxConstants.put(KeyEvent.VK_ALL_CANDIDATES, KeyCode.ALL_CANDIDATES);
        fxConstants.put(KeyEvent.VK_ALPHANUMERIC, KeyCode.ALPHANUMERIC);
        fxConstants.put(KeyEvent.VK_ALT, KeyCode.ALT);
        fxConstants.put(KeyEvent.VK_ALT_GRAPH, KeyCode.ALT_GRAPH);
        fxConstants.put(KeyEvent.VK_AMPERSAND, KeyCode.AMPERSAND);
        fxConstants.put(KeyEvent.VK_ASTERISK, KeyCode.ASTERISK);
        fxConstants.put(KeyEvent.VK_AT, KeyCode.AT);
        fxConstants.put(KeyEvent.VK_BACK_QUOTE, KeyCode.BACK_QUOTE);
        fxConstants.put(KeyEvent.VK_BACK_SLASH, KeyCode.BACK_SLASH);
        fxConstants.put(KeyEvent.VK_BACK_SPACE, KeyCode.BACK_SPACE);
        fxConstants.put(KeyEvent.VK_BEGIN, KeyCode.BEGIN);
        fxConstants.put(KeyEvent.VK_BRACERIGHT, KeyCode.BRACELEFT);
        fxConstants.put(KeyEvent.VK_CANCEL, KeyCode.CANCEL);
        fxConstants.put(KeyEvent.VK_CAPS_LOCK, KeyCode.CAPS);
        fxConstants.put(KeyEvent.VK_CIRCUMFLEX, KeyCode.CIRCUMFLEX);
        fxConstants.put(KeyEvent.VK_CLEAR, KeyCode.CLEAR);
        fxConstants.put(KeyEvent.VK_CLOSE_BRACKET, KeyCode.CLOSE_BRACKET);
        fxConstants.put(KeyEvent.VK_CODE_INPUT, KeyCode.CODE_INPUT);
        fxConstants.put(KeyEvent.VK_COLON, KeyCode.COLON);
        fxConstants.put(KeyEvent.VK_COMMA, KeyCode.COMMA);
        fxConstants.put(KeyEvent.VK_COMPOSE, KeyCode.COMPOSE);
        fxConstants.put(KeyEvent.VK_CONTEXT_MENU, KeyCode.CONTEXT_MENU);
        fxConstants.put(KeyEvent.VK_CONTROL, KeyCode.CONTROL);
        fxConstants.put(KeyEvent.VK_CONVERT, KeyCode.CONVERT);
        fxConstants.put(KeyEvent.VK_COPY, KeyCode.COPY);
        fxConstants.put(KeyEvent.VK_CUT, KeyCode.CUT);
        fxConstants.put(KeyEvent.VK_DEAD_ABOVEDOT, KeyCode.DEAD_ABOVEDOT);
        fxConstants.put(KeyEvent.VK_DEAD_ABOVERING, KeyCode.DEAD_ABOVERING);
        fxConstants.put(KeyEvent.VK_DEAD_ACUTE, KeyCode.DEAD_ACUTE);
        fxConstants.put(KeyEvent.VK_DEAD_BREVE, KeyCode.DEAD_BREVE);
        fxConstants.put(KeyEvent.VK_DEAD_CARON, KeyCode.DEAD_CARON);
        fxConstants.put(KeyEvent.VK_DEAD_CEDILLA, KeyCode.DEAD_CEDILLA);
        fxConstants.put(KeyEvent.VK_DEAD_CIRCUMFLEX, KeyCode.DEAD_CIRCUMFLEX);
        fxConstants.put(KeyEvent.VK_DEAD_DIAERESIS, KeyCode.DEAD_DIAERESIS);
        fxConstants.put(KeyEvent.VK_DEAD_DOUBLEACUTE, KeyCode.DEAD_DOUBLEACUTE);
        fxConstants.put(KeyEvent.VK_DEAD_GRAVE, KeyCode.DEAD_GRAVE);
        fxConstants.put(KeyEvent.VK_DEAD_IOTA, KeyCode.DEAD_IOTA);
        fxConstants.put(KeyEvent.VK_DEAD_MACRON, KeyCode.DEAD_MACRON);
        fxConstants.put(KeyEvent.VK_DEAD_OGONEK, KeyCode.DEAD_OGONEK);
        fxConstants.put(KeyEvent.VK_DEAD_SEMIVOICED_SOUND, KeyCode.DEAD_SEMIVOICED_SOUND);
        fxConstants.put(KeyEvent.VK_DEAD_TILDE, KeyCode.DEAD_TILDE);
        fxConstants.put(KeyEvent.VK_DEAD_VOICED_SOUND, KeyCode.DEAD_VOICED_SOUND);
        fxConstants.put(KeyEvent.VK_DECIMAL, KeyCode.DECIMAL);
        fxConstants.put(KeyEvent.VK_DELETE, KeyCode.DELETE);
        fxConstants.put(KeyEvent.VK_DOLLAR, KeyCode.DOLLAR);
        fxConstants.put(KeyEvent.VK_DOWN, KeyCode.DOWN);
        fxConstants.put(KeyEvent.VK_END, KeyCode.END);
        fxConstants.put(KeyEvent.VK_ENTER, KeyCode.ENTER);
        fxConstants.put(KeyEvent.VK_EQUALS, KeyCode.EQUALS);
        fxConstants.put(KeyEvent.VK_ESCAPE, KeyCode.ESCAPE);
        fxConstants.put(KeyEvent.VK_EURO_SIGN, KeyCode.EURO_SIGN);
        fxConstants.put(KeyEvent.VK_EXCLAMATION_MARK, KeyCode.EXCLAMATION_MARK);
        fxConstants.put(KeyEvent.VK_F1, KeyCode.F1);
        fxConstants.put(KeyEvent.VK_F2, KeyCode.F2);
        fxConstants.put(KeyEvent.VK_F3, KeyCode.F3);
        fxConstants.put(KeyEvent.VK_F4, KeyCode.F4);
        fxConstants.put(KeyEvent.VK_F5, KeyCode.F5);
        fxConstants.put(KeyEvent.VK_F6, KeyCode.F6);
        fxConstants.put(KeyEvent.VK_F7, KeyCode.F7);
        fxConstants.put(KeyEvent.VK_F8, KeyCode.F8);
        fxConstants.put(KeyEvent.VK_F9, KeyCode.F9);
        fxConstants.put(KeyEvent.VK_F10, KeyCode.F10);
        fxConstants.put(KeyEvent.VK_F11, KeyCode.F11);
        fxConstants.put(KeyEvent.VK_F12, KeyCode.F12);
        fxConstants.put(KeyEvent.VK_F13, KeyCode.F13);
        fxConstants.put(KeyEvent.VK_F14, KeyCode.F14);
        fxConstants.put(KeyEvent.VK_F15, KeyCode.F15);
        fxConstants.put(KeyEvent.VK_F16, KeyCode.F16);
        fxConstants.put(KeyEvent.VK_F17, KeyCode.F17);
        fxConstants.put(KeyEvent.VK_F18, KeyCode.F18);
        fxConstants.put(KeyEvent.VK_F19, KeyCode.F19);
        fxConstants.put(KeyEvent.VK_F20, KeyCode.F20);
        fxConstants.put(KeyEvent.VK_F21, KeyCode.F21);
        fxConstants.put(KeyEvent.VK_F22, KeyCode.F22);
        fxConstants.put(KeyEvent.VK_F23, KeyCode.F23);
        fxConstants.put(KeyEvent.VK_F24, KeyCode.F24);
        fxConstants.put(KeyEvent.VK_FINAL, KeyCode.FINAL);
        fxConstants.put(KeyEvent.VK_FIND, KeyCode.FIND);
        fxConstants.put(KeyEvent.VK_FULL_WIDTH, KeyCode.FULL_WIDTH);
        fxConstants.put(KeyEvent.VK_GREATER, KeyCode.GREATER);
        fxConstants.put(KeyEvent.VK_HALF_WIDTH, KeyCode.HALF_WIDTH);
        fxConstants.put(KeyEvent.VK_HELP, KeyCode.HELP);
        fxConstants.put(KeyEvent.VK_HIRAGANA, KeyCode.HIRAGANA);
        fxConstants.put(KeyEvent.VK_HOME, KeyCode.HOME);
        fxConstants.put(KeyEvent.VK_INPUT_METHOD_ON_OFF, KeyCode.INPUT_METHOD_ON_OFF);
        fxConstants.put(KeyEvent.VK_INSERT, KeyCode.INSERT);
        fxConstants.put(KeyEvent.VK_INVERTED_EXCLAMATION_MARK, KeyCode.INVERTED_EXCLAMATION_MARK);
        fxConstants.put(KeyEvent.VK_JAPANESE_HIRAGANA, KeyCode.JAPANESE_HIRAGANA);
        fxConstants.put(KeyEvent.VK_JAPANESE_KATAKANA, KeyCode.JAPANESE_KATAKANA);
        fxConstants.put(KeyEvent.VK_JAPANESE_ROMAN, KeyCode.JAPANESE_ROMAN);
        fxConstants.put(KeyEvent.VK_KANA, KeyCode.KANA);
        fxConstants.put(KeyEvent.VK_KANA_LOCK, KeyCode.KANA_LOCK);
        fxConstants.put(KeyEvent.VK_KANJI, KeyCode.KANJI);
        fxConstants.put(KeyEvent.VK_KATAKANA, KeyCode.KATAKANA);
        fxConstants.put(KeyEvent.VK_KP_DOWN, KeyCode.KP_DOWN);
        fxConstants.put(KeyEvent.VK_KP_LEFT, KeyCode.KP_LEFT);
        fxConstants.put(KeyEvent.VK_KP_RIGHT, KeyCode.KP_RIGHT);
        fxConstants.put(KeyEvent.VK_KP_UP, KeyCode.KP_UP);
        fxConstants.put(KeyEvent.VK_LEFT, KeyCode.LEFT);
        fxConstants.put(KeyEvent.VK_LEFT_PARENTHESIS, KeyCode.LEFT_PARENTHESIS);
        fxConstants.put(KeyEvent.VK_LESS, KeyCode.LESS);
        fxConstants.put(KeyEvent.VK_META, KeyCode.META);
        fxConstants.put(KeyEvent.VK_MINUS, KeyCode.MINUS);
        fxConstants.put(KeyEvent.VK_MODECHANGE, KeyCode.MODECHANGE);
        fxConstants.put(KeyEvent.VK_MULTIPLY, KeyCode.MULTIPLY);
        fxConstants.put(KeyEvent.VK_NONCONVERT, KeyCode.NONCONVERT);
        fxConstants.put(KeyEvent.VK_NUM_LOCK, KeyCode.NUM_LOCK);
        fxConstants.put(KeyEvent.VK_NUMBER_SIGN, KeyCode.NUMBER_SIGN);
        fxConstants.put(KeyEvent.VK_NUMPAD0, KeyCode.NUMPAD0);
        fxConstants.put(KeyEvent.VK_NUMPAD1, KeyCode.NUMPAD1);
        fxConstants.put(KeyEvent.VK_NUMPAD2, KeyCode.NUMPAD2);
        fxConstants.put(KeyEvent.VK_NUMPAD3, KeyCode.NUMPAD3);
        fxConstants.put(KeyEvent.VK_NUMPAD4, KeyCode.NUMPAD4);
        fxConstants.put(KeyEvent.VK_NUMPAD5, KeyCode.NUMPAD5);
        fxConstants.put(KeyEvent.VK_NUMPAD6, KeyCode.NUMPAD6);
        fxConstants.put(KeyEvent.VK_NUMPAD7, KeyCode.NUMPAD7);
        fxConstants.put(KeyEvent.VK_NUMPAD8, KeyCode.NUMPAD8);
        fxConstants.put(KeyEvent.VK_NUMPAD9, KeyCode.NUMPAD9);
        fxConstants.put(KeyEvent.VK_OPEN_BRACKET, KeyCode.OPEN_BRACKET);
        fxConstants.put(KeyEvent.VK_PAGE_DOWN, KeyCode.PAGE_DOWN);
        fxConstants.put(KeyEvent.VK_PAGE_UP, KeyCode.PAGE_UP);
        fxConstants.put(KeyEvent.VK_PASTE, KeyCode.PASTE);
        fxConstants.put(KeyEvent.VK_PAUSE, KeyCode.PAUSE);
        fxConstants.put(KeyEvent.VK_PERIOD, KeyCode.PERIOD);
        fxConstants.put(KeyEvent.VK_PLUS, KeyCode.PLUS);
        fxConstants.put(KeyEvent.VK_PREVIOUS_CANDIDATE, KeyCode.PREVIOUS_CANDIDATE);
        fxConstants.put(KeyEvent.VK_PRINTSCREEN, KeyCode.PRINTSCREEN);
        fxConstants.put(KeyEvent.VK_PROPS, KeyCode.PROPS);
        fxConstants.put(KeyEvent.VK_QUOTE, KeyCode.QUOTE);
        fxConstants.put(KeyEvent.VK_QUOTEDBL, KeyCode.QUOTEDBL);
        fxConstants.put(KeyEvent.VK_RIGHT, KeyCode.RIGHT);
        fxConstants.put(KeyEvent.VK_RIGHT_PARENTHESIS, KeyCode.RIGHT_PARENTHESIS);
        fxConstants.put(KeyEvent.VK_ROMAN_CHARACTERS, KeyCode.ROMAN_CHARACTERS);
        fxConstants.put(KeyEvent.VK_SCROLL_LOCK, KeyCode.SCROLL_LOCK);
        fxConstants.put(KeyEvent.VK_SEMICOLON, KeyCode.SEMICOLON);
        fxConstants.put(KeyEvent.VK_SEPARATOR, KeyCode.SEPARATOR);
        fxConstants.put(KeyEvent.VK_SHIFT, KeyCode.SHIFT);
        fxConstants.put(KeyEvent.VK_SLASH, KeyCode.SLASH);
        fxConstants.put(KeyEvent.VK_SPACE, KeyCode.SPACE);
        fxConstants.put(KeyEvent.VK_STOP, KeyCode.STOP);
        fxConstants.put(KeyEvent.VK_SUBTRACT, KeyCode.SUBTRACT);
        fxConstants.put(KeyEvent.VK_TAB, KeyCode.TAB);
        fxConstants.put(KeyEvent.VK_UNDEFINED, KeyCode.UNDEFINED);
        fxConstants.put(KeyEvent.VK_UNDERSCORE, KeyCode.UNDERSCORE);
        fxConstants.put(KeyEvent.VK_UNDO, KeyCode.UNDO);
        fxConstants.put(KeyEvent.VK_UP, KeyCode.UP);
        fxConstants.put(KeyEvent.VK_WINDOWS, KeyCode.WINDOWS);
    }

    /**
     * Returns the AWT KeyCode from FX KeyCode.
     * @param keyCode The FX KeyCode.
     * @return The AWT KeyCode.
     */
    public static int toAWTKeyEvent(KeyCode keyCode) {
        if (keyCode.getName().length() > 1) {
            return awtConstants.getOrDefault(keyCode, KeyEvent.VK_UNDEFINED);
        }

        return KeyEvent.getExtendedKeyCodeForChar(keyCode.getName().charAt(0));
    }

    /**
     * Returns the FX KeyCode from AWT KeyCode.
     * @param keyCode The AWT KeyCode.
     * @return The FX KeyCode.
     */
    public static KeyCode toFxKeyCode(int keyCode) {
        if (fxConstants.containsKey(keyCode)) return fxConstants.get(keyCode);

        return KeyCode.getKeyCode(KeyEvent.getKeyText(keyCode));
    }
}
