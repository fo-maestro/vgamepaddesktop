/*
 * PayerQueue.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */

package com.fomaster.vgamepad.control.util;

import com.fomaster.vgamepad.control.Player;
import com.fomaster.vgamepad.thread.ThreadListener;

import java.util.*;

/**
 * Synchronized Player List dispatched in Queue Mode.
 *
 * @author Felipe Oyarzun
 * @version 1.0
 */
public class PlayerQueue {

    public static final int MAX_PLAYERS = 4;

    private PriorityQueue<Integer> playerQueue;
    private Player[] players;
    private ThreadListener<Player> mListener;

    /**
     * Create a Player List with the specific number of players.
     * @param nPlayers The number of players.
     */
    public PlayerQueue(int nPlayers) throws PlayerLimitException {
        if (nPlayers > MAX_PLAYERS) {
            throw new PlayerLimitException("The number of player is higher than 4");
        }

        players = new Player[nPlayers];
        playerQueue = new PriorityQueue<>();
        List<Integer> list = new ArrayList<>();

        for (int i = 0; i < nPlayers; i++) {
            list.add(i);
        }

        playerQueue.addAll(list);
    }

    /**
     * Set the thread listener for all players.
     * @param listener The thread listener.
     */
    public void setOnThreadFinishListener(ThreadListener<Player> listener) {
        mListener = listener;
    }

    /**
     * Add a new player in the list.
     * @param player The player that will be added.
     */
    public synchronized int add(Player player) {
        int index = playerQueue.poll();
        players[index] = player;

        if (mListener != null) {
            player.setOnThreadFinishListener(mListener);
        }

        player.start();

        return index;
    }

    /**
     * Get the player number available in the list.
     * @return The player number available in the list.
     */
    public synchronized int peekAvailablePlayerNum() {
        return playerQueue.peek();
    }

    /**
     * Get the available number of players.
     * @return The available numbers of players.
     */
    public synchronized int getAvailablePlayers() {
        return playerQueue.size();
    }

    /**
     * Get the number of players int the list.
     * @return The number of players in the list.
     */
    public synchronized int getCurrentPlayers() {
        return players.length - getAvailablePlayers();
    }

    /**
     * Get the player in the specific index of the list.
     * @param index The index of the list.
     * @return The player.
     */
    public synchronized Player get(int index) {
        return (index < players.length) ? players[index] : null;
    }

    /**
     * Remove the player in the position indicated by the index.
     * @param index The index of the player.
     * @return True if the player has been deleted successfully.
     */
    public synchronized boolean remove(int index) {
        if (index >= players.length) {
            return false;
        }

        players[index] = null;
        playerQueue.offer(index);

        return true;
    }

    /**
     * Close and delete all players in the list.
     */
    public void closeAll() {
        for (int i = 0; i < players.length; i++) {
            if (players[i] != null) {
                players[i].closeClient();
            }
        }
    }
}
