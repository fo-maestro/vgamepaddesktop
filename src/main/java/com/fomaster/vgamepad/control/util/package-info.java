/**
 * Utils classes used to manage the controls of a player or a suite of players.
 *
 * @author Felipe Oyarzun
 * @version 1.0, 06-05-2017
 */
package com.fomaster.vgamepad.control.util;