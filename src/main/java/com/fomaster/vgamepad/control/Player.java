/*
 * Player.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */

package com.fomaster.vgamepad.control;

import com.fomaster.vgamepad.thread.ThreadListener;

import javax.microedition.io.StreamConnection;
import java.awt.*;
import java.io.DataInputStream;
import java.io.IOException;
import java.util.HashMap;

/**
 * Device class controller.
 *
 * @author Felipe Oyarzun
 * @version 1.0
 */
public class Player extends Thread {

    private StreamConnection client;
    private DataInputStream clientInput;
    private ThreadListener<Player> mListener;
    private HashMap<Integer, Integer> keymap;
    private boolean running;
    private int playerNum;

    /**
     * Create a Player with a specific player number and their stream connection
     * with the android device.
     * @param playerNum The player number.
     * @param client The stream connection.
     */
    public Player(int playerNum, StreamConnection client) {
        this.client = client;
        this.playerNum = playerNum;
    }

    /**
     * Get the player number associated.
     * @return The player number.
     */
    public int getPlayerNum() { return playerNum; }

    @Override
    public void run() {
        try {
            clientInput = client.openDataInputStream();
            try {
                Robot robot = new Robot();
                running = true;
                while (running) {
                    int res = clientInput.readInt();
                    int key = keymap.get(GamePadConstants.getKeyWithOutState(res));

                    if (GamePadConstants.isKeyPressed(res)) {
                        robot.keyPress(key);
                    } else {
                        robot.keyRelease(key);
                    }
                }
            } catch (AWTException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (mListener != null) mListener.threadFinish(this);
    }

    /**
     * Close the client stream connection and terminate the thread.
     */
    public void closeClient() {
        try {
            running = false;
            clientInput.close();
            client.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Set the thread finish listener for the client.
     * @param listener The thread finish listener.
     */
    public void setOnThreadFinishListener(ThreadListener<Player> listener) {
        mListener = listener;
    }
}
