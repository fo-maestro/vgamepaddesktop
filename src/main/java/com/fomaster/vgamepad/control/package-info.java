/**
 * Control and utility classes for the android app and elemental handler.
 *
 * @author Felipe Oyarzun
 * @version 1.0
 */
package com.fomaster.vgamepad.control;