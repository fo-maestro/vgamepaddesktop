/*
 * ThreadListener.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */

package com.fomaster.vgamepad.thread;

/**
 * Contains all KeyMaps(AWT and FX) and Key transformers methods from AWT/FX to FX/AWT
 *
 * @author Felipe Oyarzun
 * @version 1.0, 11-01-2017
 */
public interface ThreadListener<T> {
    void threadFinish(T caller);
}
