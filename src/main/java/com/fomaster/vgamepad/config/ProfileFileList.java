package com.fomaster.vgamepad.config;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

/**
 * Created by felip_000 on 15-03-2017.
 */
public class ProfileFileList extends ArrayList<ProfileFile> {

    public ProfileFile get(String profileName) {
        for (ProfileFile element : this) {
            if (element.getProfileName().equals(profileName)) return element;
        }

        return null;
    }

    public boolean remove(String profileName) {
        for (int i = 0; i < size(); i++) {
            if (get(i).getProfileName().equals(profileName)) {
                super.remove(i);

                return true;
            }
        }

        return false;
    }

    public boolean removeOnDisk(String profileName) {
        try {
            if (Files.deleteIfExists(Paths.get(ProfileFile.DIR_PATH + "/" + ProfileFile.filenameConvert(profileName) + "." + ProfileFile.FILE_EXTENSION))) {
                if (this.remove(profileName)) {
                    return true;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }
}
