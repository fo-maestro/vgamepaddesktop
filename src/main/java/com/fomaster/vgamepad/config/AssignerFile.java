/*
 * AssignerFile.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */

package com.fomaster.vgamepad.config;

import java.io.IOException;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public class AssignerFile implements ConfigFile {


    public static final class Builder implements ConfigFile.Builder<AssignerFile> {

        @Override
        public AssignerFile build() {
            return null;
        }
    }
}
