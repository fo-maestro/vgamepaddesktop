/*
 * ConfigFile.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */

package com.fomaster.vgamepad.config;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public interface ConfigFile extends Serializable {

    static Object load(String filePath) throws IOException, ClassNotFoundException {
        ObjectInputStream in = new ObjectInputStream(new FileInputStream(filePath));
        Object obj = in.readObject();

        if (obj instanceof ConfigFile) {
            return obj;
        }

        return null;
    }

    interface Builder<T> {
        T build();
    }
}
