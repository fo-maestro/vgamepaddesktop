package com.fomaster.vgamepad.config;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;

/**
 * Created by felip_000 on 05-02-2017.
 */
public final class ProfileFile implements ConfigFile {

    private final String profileName;
    private final HashMap<Integer, Integer> keyMap;
    public static final String DIR_PATH = "profiles";
    public static final String FILE_EXTENSION = "cfg";

    static {
        if (Files.notExists(Paths.get(DIR_PATH))) {
            boolean result = new File(DIR_PATH).mkdir();
            System.out.println(
                    result ? "\"" + DIR_PATH + "\"" + " Create" : "Failed to create \"" + DIR_PATH + "\"");
        }
    }

    private ProfileFile(Builder builder) {
        profileName = builder.profileName;
        keyMap = builder.keyMap;
    }

    public static String filenameConvert(String profileName) {
        StringBuilder result = new StringBuilder();

        if (profileName != null) {
            for (int i = 0; i < profileName.length(); i++) {
                result.append((profileName.charAt(i) != ' ') ? profileName.charAt(i) : "_");
            }
        }

        return result.toString().toLowerCase();
    }

    public String getProfileName() {
        return profileName;
    }

    @SuppressWarnings("unchecked")
    public HashMap<Integer, Integer> getKeyMap() {
        return (HashMap<Integer, Integer>) keyMap.clone();
    }

    public static final class Builder implements ConfigFile.Builder<ProfileFile> {

        private String profileName;
        private HashMap<Integer, Integer> keyMap;

        public Builder() {
            profileName = "default";
            keyMap = new HashMap<>();
        }

        public Builder setProfileName(String profileName) {
            if (profileName != null) {
                this.profileName = profileName;
            }

            return this;
        }

        @SuppressWarnings("unchecked")
        public Builder setKeyMap(HashMap<Integer, Integer> keyMap) {
            if (keyMap != null) {
                this.keyMap = (HashMap<Integer, Integer>) keyMap.clone();
            }

            return this;
        }

        @Override
        public ProfileFile build() {
            ProfileFile file = new ProfileFile(this);

            try {
                ObjectOutputStream out = new ObjectOutputStream(
                        new FileOutputStream(
                                DIR_PATH + "/" + filenameConvert(profileName) + "." + FILE_EXTENSION));

                out.writeObject(file);
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return file;
        }
    }
}
