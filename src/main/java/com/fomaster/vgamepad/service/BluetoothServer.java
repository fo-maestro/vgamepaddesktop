package com.fomaster.vgamepad.service;

import com.fomaster.vgamepad.control.Player;
import com.fomaster.vgamepad.control.util.PlayerQueue;
import com.fomaster.vgamepad.config.ProfileFileList;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

import javax.microedition.io.Connector;
import javax.microedition.io.StreamConnectionNotifier;
import java.io.IOException;

/**
 * Created by felip_000 on 09-01-2017.
 */
public class BluetoothServer extends Thread {

    public static final String UUID = "0000110100001000800000805F9B34FB";
    public static final String URL = "btspp://localhost:" + UUID + ";name=VGamePadServer";
    private StreamConnectionNotifier server;
    private int numPlayers;
    private final PlayerQueue playerQueue;
    private ProfileFileList profiles;
    private boolean running;
    private Rectangle[] playerState;

    public BluetoothServer(int numPlayers, Rectangle[] playerState) {
        this.numPlayers = numPlayers;
        this.playerState = playerState;
        playerQueue = new PlayerQueue(numPlayers);
        playerQueue.setOnThreadFinishListener(caller -> {
            int index = caller.getPlayerNum();
            playerState[index].setFill(Color.RED);
            playerQueue.remove(index);

            synchronized (BluetoothServer.this) {
                BluetoothServer.this.notify();
            }
        });
    }

    @Override
    public void run() {
        running = true;

        try {
            System.out.println("Starting Server...");
            server = (StreamConnectionNotifier) Connector.open(URL);
            System.out.println("Attempt to connect");
        } catch (IOException e) {
            e.printStackTrace();
        }

        while (running) {
            try {
                if (playerQueue.getAvailablePlayers() == 0) {
                    synchronized (this) {
                        try {
                            wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }

                System.out.println("Wait Connection");
                int index = playerQueue.add(new Player(playerQueue.peekAvailablePlayerNum(), server.acceptAndOpen()));
                playerState[index].setFill(Color.SPRINGGREEN);
                System.out.println("Player Connected");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        System.out.println("Close server");
    }

    public void close() {
        running = false;
        playerQueue.closeAll();

        try {
            server.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
