package com.fomaster.vgamepad.view;

import com.fomaster.vgamepad.config.ConfigFile;
import com.fomaster.vgamepad.control.util.KeyUtils;
import com.fomaster.vgamepad.config.ProfileFile;
import com.fomaster.vgamepad.config.ProfileFileList;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;

/**
 * Created by felip_000 on 09-03-2017.
 */
public class SplashScreen {

    private Stage primaryStage;
    private ProfileFileList profiles;
    private HashSet<KeyCode> usedKeys;

    private Runnable initTask = (() -> {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Load Config Files");
        profiles = new ProfileFileList();
        usedKeys = new HashSet<>();
        File files = new File(ProfileFile.DIR_PATH);
        File[] fileList = files.listFiles();

        if (fileList != null) {
            for (File element : fileList) {
                if (!element.isDirectory()) {
                    if (element.getName().substring(element.getName().lastIndexOf(".") + 1).equals(ProfileFile.FILE_EXTENSION)) {
                        try {
                            ProfileFile config =
                                    (ProfileFile) ConfigFile.load(ProfileFile.DIR_PATH + "/" + element.getName());
                            if (config != null) {
                                profiles.add(config);
                                config.getKeyMap().forEach((key, value) -> usedKeys.add(KeyUtils.toFxKeyCode(value)));
                            }
                        } catch (IOException | ClassNotFoundException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            System.out.println("End Load");

            Platform.runLater(() -> {
                Stage appStage = new Stage();
                appStage.getIcons().add(new Image(getClass().getResource("../resources/app_logo.png").toExternalForm()));
                appStage.setResizable(false);
                appStage.setOnCloseRequest(event -> System.exit(0));
                FXMLLoader loader = new FXMLLoader(getClass().getResource("root.fxml"));
                try {
                    Parent content = loader.load();
                    RootController controller = loader.getController();
                    controller.setData(profiles, usedKeys);
                    appStage.setScene(new Scene(content));
                    primaryStage.close();
                    appStage.show();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        }
    });

    public SplashScreen(Stage primaryStage) {
        this.primaryStage = primaryStage;
        primaryStage.getIcons().add(new Image(getClass().getResource("../resources/app_logo.png").toExternalForm()));
        primaryStage.initStyle(StageStyle.UNDECORATED);
        try {
            primaryStage.setScene(new Scene(FXMLLoader.load(getClass().getResource("splash_screen.fxml"))));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private double computeCenter(double a, double b) {
        return (a - b) / 2;
    }

    public void show() {
        primaryStage.show();
        Rectangle2D screenBounds = Screen.getPrimary().getVisualBounds();
        primaryStage.setX(computeCenter(screenBounds.getWidth(), primaryStage.getWidth()));
        primaryStage.setY(computeCenter(screenBounds.getHeight(), primaryStage.getHeight()));
        new Thread(initTask).start();
    }
}
