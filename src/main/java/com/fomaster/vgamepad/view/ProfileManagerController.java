package com.fomaster.vgamepad.view;

import com.fomaster.vgamepad.control.util.KeyUtils;
import com.fomaster.vgamepad.control.GamePadConstants;
import com.fomaster.vgamepad.config.ProfileFile;
import com.fomaster.vgamepad.config.ProfileFileList;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by felip_000 on 05-02-2017.
 */
public class ProfileManagerController {

    private Dialog<Void> context;
    @FXML private VBox parentLeft;
    @FXML private VBox parentRight;
    @FXML private ComboBox<String> profiles;
    @FXML private Button save;

    private ProfileFileList profileList;
    private HashMap<Integer, Integer> keymap;
    private HashSet<KeyCode> usedKeys;
    private HashSet<KeyCode> temp;
    private Alert profileError;
    private Alert profileCreation;
    private TextInputDialog profileNameDialog;
    private String newProfileName;
    private String prevProfileName;

    @FXML private void initialize() {
        keymap = new HashMap<>();
        prevProfileName = "";
        newProfileName = "";

        initializeButtons(parentLeft);
        initializeButtons(parentRight);

        //init dialogs
        profileError = new Alert(Alert.AlertType.ERROR);
        profileError.setTitle("Profile Operation");
        profileError.setHeaderText("Profile Error");
        profileError.setContentText("No profile selected");

        profileCreation = new Alert(Alert.AlertType.ERROR);
        profileCreation.setTitle("Profile");

        profileNameDialog = new TextInputDialog();
        ImageView graphic =
                new ImageView(getClass().getResource("../resources/profile_joy.png").toString());

        graphic.setFitWidth(50);
        graphic.setFitHeight(50);
        profileNameDialog.setGraphic(graphic);
        profileNameDialog.setContentText("Name:");
    }

    private void initializeButtons(VBox containerButtons) {
        for (Node element : containerButtons.getChildren()) {
            if (element instanceof HBox) {
                HBox container = (HBox) element;
                for (Node elem : container.getChildren()) {
                    if (elem instanceof Button) {
                        ((Button) elem).setOnAction(event -> handleButtonClick((Button) elem));
                    }
                }
            }
        }
    }

    private void initializeComboBox() {
        profileList.forEach(configFile -> profiles.getItems().add(configFile.getProfileName()));
        changeProfile(0);
    }

    private void loadKeys(HashMap<Integer, Integer> keymap) {
        loadKeys(parentLeft, keymap);
        loadKeys(parentRight, keymap);
    }

    private void loadKeys(VBox containerButtons, HashMap<Integer, Integer> keymap) {
        for (Node element : containerButtons.getChildren()) {
            if (element instanceof HBox) {
                HBox container = (HBox) element;
                Label label = (Label) container.getChildren().get(0);
                Button btn = (Button) container.getChildren().get(1);
                Integer result = keymap.get(getKeyCode(btn.getText()));

                if (result != null) {
                    KeyCode code = KeyUtils.toFxKeyCode(result);
                    label.setText(code.getName());
                } else {
                    label.setText("");
                }
            }
        }
    }

    void setContext(Dialog<Void> context) {
        this.context = context;
    }

    @SuppressWarnings("unchecked")
    void setData(ProfileFileList profiles, HashSet<KeyCode> usedKeys) {
        profileList = profiles;
        this.usedKeys = usedKeys;

        if (!profiles.isEmpty()) {
            initializeComboBox();
        }
    }

    @FXML private void handleButtonClose() {
        Stage stage = (Stage) context.getDialogPane().lookupButton(ButtonType.CLOSE).getScene().getWindow();
        stage.close();
    }

    @FXML private void handleButtonAdd() {
        profileNameDialog.getEditor().clear();
        profileNameDialog.setTitle("Add Profile");
        profileNameDialog.setHeaderText("Enter Your Profile Name");
        Platform.runLater(profileNameDialog.getEditor()::requestFocus);

        Optional<String> result = profileNameDialog.showAndWait();

        result.ifPresent(consumer -> {

            if (!consumer.isEmpty()) {
                ObservableList<String> items = profiles.getItems();

                if (isDuplicateProfile(items, consumer)) return;

                items.add(consumer);
                changeProfile(items.size() - 1);
            } else {
                profileCreation.setHeaderText("Profile creation error");
                profileCreation.setContentText("The profile name is empty");
                profileCreation.showAndWait();
            }
        });
    }

    private boolean isDuplicateProfile(ObservableList<String> items, String consumer) {
        if (!items.isEmpty()) {
            for (String element : items) {
                if (element.compareToIgnoreCase(consumer) == 0) {
                    profileCreation.setContentText("The profile already exists");
                    profileCreation.showAndWait();

                    return true;
                }
            }
        }

        return false;
    }

    @SuppressWarnings("unchecked")
    private void changeProfile(int index) {
        temp = (HashSet<KeyCode>) usedKeys.clone();
        keymap.clear();
        ProfileFile config = profileList.get(profiles.getItems().get(index));

        if (config != null) keymap.putAll(config.getKeyMap());

        loadKeys(keymap);
        profiles.getSelectionModel().select(index);
    }

    @FXML private void handleButtonDelete () {
        if (profiles.getItems().isEmpty()) {
            profileError.showAndWait();
            return;
        }

        Alert dialog = new Alert(Alert.AlertType.CONFIRMATION);
        dialog.setTitle("Delete Profile");
        dialog.setHeaderText("Delete Profile");
        dialog.setContentText("Are you sure to delete this profile?");

        Optional<ButtonType> result = dialog.showAndWait();


        result.ifPresent(buttonType -> {
            if (buttonType == ButtonType.OK) {
                Collection<Integer> used = keymap.values();
                String strRemove = profiles.getSelectionModel().getSelectedItem();

                Collection<KeyCode> strList = new ArrayList<>();

                used.forEach(consumer -> strList.add(KeyUtils.toFxKeyCode(consumer)));

                usedKeys.removeAll(strList);

                if (profiles.getItems().size() > 1) {
                    if (profiles.getSelectionModel().getSelectedIndex() == 0) {
                        changeProfile(profiles.getSelectionModel().getSelectedIndex() + 1);
                    } else if (profiles.getSelectionModel().getSelectedIndex() == profiles.getItems().size() - 1) {
                        changeProfile(profiles.getSelectionModel().getSelectedIndex() - 1);
                    }
                } else {
                    keymap.clear();
                    loadKeys(keymap);
                }

                EventHandler<ActionEvent> listener = profiles.getOnAction();
                profiles.setOnAction(null);
                profiles.getItems().remove(strRemove);
                profiles.setOnAction(listener);
                profileList.removeOnDisk(strRemove);

                profiles.requestLayout();
            }
        });
    }

    @FXML private void handleButtonEdit() {
        profileNameDialog.getEditor().clear();
        profileNameDialog.setTitle("Edit Profile");
        profileNameDialog.setHeaderText("Enter a new name profile:");

        Optional<String> result = profileNameDialog.showAndWait();

        result.ifPresent(consumer -> {
            if (!consumer.isEmpty()) {
                ObservableList<String> items = profiles.getItems();

                if (isDuplicateProfile(items, consumer)) return;

                prevProfileName = items.get(profiles.getSelectionModel().getSelectedIndex());
                newProfileName = consumer;
                EventHandler<ActionEvent> listener = profiles.getOnAction();
                profiles.setOnAction(null);
                items.set(profiles.getSelectionModel().getSelectedIndex(), consumer);
                profiles.getSelectionModel().select(consumer);
                profiles.setOnAction(listener);

                save.setDisable(false);
            } else {
                profileCreation.setHeaderText("Profile edit error");
                profileCreation.setContentText("The profile name is empty");
                profileCreation.showAndWait();
            }
        });
    }

    private void handleButtonClick(Button caller) {
        if (profiles.getItems().isEmpty()) {
            profileError.showAndWait();
            return;
        }

        int index = 0;

        for (Node current : caller.getParent().getChildrenUnmodifiable()) {
            if (current instanceof Label) {
                index = caller.getParent().getChildrenUnmodifiable().indexOf(current);
            }
        }

        final Label keyBinding = (Label) caller.getParent().getChildrenUnmodifiable().get(index);

        String message = "Press a key to set or press ESC to exit...";
        Alert dialog = new Alert(Alert.AlertType.NONE);
        dialog.initStyle(StageStyle.UNDECORATED);
        dialog.getDialogPane().setContentText(message);
        dialog.getDialogPane().getButtonTypes().add(ButtonType.CLOSE);
        dialog.getDialogPane().requestFocus();
        Node close = dialog.getDialogPane().lookupButton(ButtonType.CLOSE);
        close.managedProperty().bind(close.visibleProperty());
        close.setVisible(false);

        Thread counter = new Thread(() -> {
            AtomicInteger time = new AtomicInteger(5);

            try {
                while (time.get() != 0) {
                    Platform.runLater(() ->
                            dialog.getDialogPane().setContentText(message + " " + time.getAndDecrement()));

                    Thread.sleep(1000);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            Platform.runLater(dialog::close);
        });

        dialog.getDialogPane().setOnKeyPressed(event -> {
            boolean error = false;

            if (event.getCode() != KeyCode.ESCAPE) {
                if (temp.add(event.getCode())) {

                    if (!keyBinding.getText().isEmpty()) {
                        temp.remove(KeyCode.getKeyCode(keyBinding.getText()));
                    }

                    keyBinding.setText(event.getCode().getName());
                    keymap.put(getKeyCode(caller.getText()), KeyUtils.toAWTKeyEvent(event.getCode()));

                    if (!profiles.getItems().isEmpty()) save.setDisable(false);
                } else {
                    error = true;
                }
            }

            counter.interrupt();

            if (error) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Assigned Key");
                alert.setHeaderText("Assigned Key Error");
                alert.setContentText("The key " + event.getText() + " already exist");
                alert.showAndWait();
            }
        });

        counter.start();
        dialog.showAndWait();
    }

    @FXML private void handleComboChange() {
        if (!prevProfileName.isEmpty()) {
            profiles.getItems().set(profiles.getItems().indexOf(newProfileName), prevProfileName);
            prevProfileName = "";
            newProfileName = "";
        }

        changeProfile(profiles.getSelectionModel().getSelectedIndex());
        save.setDisable(true);
    }

    @FXML private void handleButtonSave() {
        boolean status = profileList.removeOnDisk(
                prevProfileName.isEmpty() ? profiles.getSelectionModel().getSelectedItem() : prevProfileName);

        System.out.println(status);
        ProfileFile result =
                new ProfileFile.Builder()
                        .setProfileName(profiles.getSelectionModel().getSelectedItem())
                        .setKeyMap(keymap)
                        .build();

        usedKeys.addAll(temp);

        profileList.add(result);
        prevProfileName = "";
        newProfileName = "";

        save.setDisable(true);
    }

    private int getKeyCode(String fxButton) {
        switch (fxButton.toUpperCase()) {
            case "L2":
                return GamePadConstants.BUTTON_L2;

            case "L1":
                return GamePadConstants.BUTTON_L1;

            case "SELECT":
                return GamePadConstants.BUTTON_SELECT;

            case "D-LEFT":
                return GamePadConstants.D_PAD_LEFT;

            case "D-RIGHT":
                return GamePadConstants.D_PAD_RIGHT;

            case "D-UP":
                return GamePadConstants.D_PAD_UP;

            case "D-DOWN":
                return GamePadConstants.D_PAD_DOWN;

            case "R2":
                return GamePadConstants.BUTTON_R2;

            case "R1":
                return GamePadConstants.BUTTON_R1;

            case "START":
                return GamePadConstants.BUTTON_START;

            case "X":
                return GamePadConstants.BUTTON_PAD_X;

            case "B":
                return GamePadConstants.BUTTON_PAD_B;

            case "Y":
                return GamePadConstants.BUTTON_PAD_Y;

            case "A":
                return GamePadConstants.BUTTON_PAD_A;

            default:
                return KeyEvent.VK_UNDEFINED;
        }
    }
}
