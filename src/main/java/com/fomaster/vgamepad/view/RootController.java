package com.fomaster.vgamepad.view;

import com.fomaster.vgamepad.service.BluetoothServer;
import com.fomaster.vgamepad.util.BluetoothAddress;
import com.fomaster.vgamepad.config.ProfileFileList;
import javafx.collections.FXCollections;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

import javax.bluetooth.BluetoothStateException;
import javax.bluetooth.LocalDevice;
import java.util.HashSet;
import java.util.Optional;

/**
 * Created by felip_000 on 10-01-2017.
 */
public class RootController {

    @FXML private ListView<String> playerList;
    @FXML private Label bluetoothStats;
    @FXML private MenuItem openServerMI;
    @FXML private MenuItem closeServerMI;
    @FXML private Rectangle player1R;
    @FXML private Rectangle player2R;
    @FXML private Rectangle player3R;
    @FXML private Rectangle player4R;
    @FXML private Rectangle serverStatus;

    private Rectangle[] mPlayerState;
    private BluetoothServer server;
    private boolean bluetoothEnable;
    private ProfileFileList profiles;
    private HashSet<KeyCode> usedKeys;

    @FXML private void initialize() {
        playerList.setItems(FXCollections.observableArrayList("Player 1", "Player 2", "Player 3", "Player 4"));
        mPlayerState = new Rectangle[]{player1R, player2R, player3R, player4R};

        try {
            bluetoothStats.setText(BluetoothAddress.format(LocalDevice.getLocalDevice().getBluetoothAddress()));
            System.out.println(LocalDevice.getLocalDevice().getBluetoothAddress());
            bluetoothEnable = true;
        } catch (BluetoothStateException e) {
           bluetoothStats.setText("Bluetooth Off");
           bluetoothEnable = false;
        }

    }

    void setData(ProfileFileList profiles, HashSet<KeyCode> usedKeys) {
        this.profiles = profiles;
        this.usedKeys = usedKeys;
    }

    @FXML private void handleOpenServerMI() {
        if (!bluetoothEnable) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Bluetooth Disabled");
            alert.setHeaderText(null);
            alert.setContentText("Bluetooth Disabled, please turn on the Device.");
            alert.showAndWait();
            return;
        }

        Dialog<ButtonType> serverDialog = new Dialog<>();
        serverDialog.setTitle("Open Server");
        serverDialog.setHeaderText("Open Bluetooth Server Connection");
        ImageView graphic = new ImageView(getClass().getResource("../resources/bluetooth.png").toString());
        graphic.setFitWidth(50);
        graphic.setFitHeight(50);
        serverDialog.setGraphic(graphic);

        HBox content = new HBox();
        content.setSpacing(5);

        Label numLabel = new Label("Players:");
        ComboBox<Integer> numPlayers = new ComboBox<>(FXCollections.observableArrayList(1, 2, 3, 4));
        numPlayers.getSelectionModel().selectFirst();

        content.getChildren().addAll(numLabel, numPlayers);

        serverDialog.getDialogPane().setContent(content);
        ButtonType open = new ButtonType("Open", ButtonBar.ButtonData.OK_DONE);
        serverDialog.getDialogPane().getButtonTypes().addAll(open, ButtonType.CANCEL);
        Optional<ButtonType> response = serverDialog.showAndWait();
        response.ifPresent(consumer -> {
            if (consumer.equals(open)) {
                server = new BluetoothServer(numPlayers.getSelectionModel().getSelectedItem(), mPlayerState);
                server.start();
                serverStatus.setFill(Color.SPRINGGREEN);
                closeServerMI.setDisable(false);
                openServerMI.setDisable(true);
            }
        });
    }

    @FXML private void handleCloseServerMI() {
        Alert dialog = new Alert(Alert.AlertType.CONFIRMATION);
        dialog.setTitle("Close Server");
        dialog.setHeaderText("Close Server");
        dialog.setContentText("Are you sure to Close Server?");
        Optional<ButtonType> result = dialog.showAndWait();

        result.ifPresent(consumer -> {
            if (consumer.equals(ButtonType.OK)) {
                server.close();
                serverStatus.setFill(Color.RED);
                closeServerMI.setDisable(true);
                openServerMI.setDisable(false);
            }
        });
    }

    @FXML private void handleExitMI() {
        System.exit(0);
    }

    @FXML private void handleNewProfileMI() throws Exception {
        Dialog<Void> dialog = new Dialog<>();
        dialog.setOnCloseRequest(Event::consume);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("profile_manager.fxml"));
        dialog.getDialogPane().setContent(loader.load());
        ProfileManagerController controller = loader.getController();
        dialog.getDialogPane().getButtonTypes().add(ButtonType.CLOSE);
        Node closeButton = dialog.getDialogPane().lookupButton(ButtonType.CLOSE);
        closeButton.managedProperty().bind(closeButton.visibleProperty());
        closeButton.setVisible(false);
        controller.setContext(dialog);
        controller.setData(profiles, usedKeys);
        dialog.showAndWait();
    }

    @FXML private void handleAssignProfile() throws Exception {
        Dialog<Void> dialog = new Dialog<>();
        dialog.setOnCloseRequest(Event::consume);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("assign.fxml"));
        dialog.getDialogPane().setContent(loader.load());
        AssignController controller = loader.getController();
        dialog.getDialogPane().getButtonTypes().add(ButtonType.CLOSE);
        Node closeButton = dialog.getDialogPane().lookupButton(ButtonType.CLOSE);
        closeButton.managedProperty().bind(closeButton.visibleProperty());
        closeButton.setVisible(false);
        dialog.showAndWait();
    }
}
