/*
 * AssignControler.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */

package com.fomaster.vgamepad.view;

import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public class AssignController {

    @FXML private VBox root;
    private List<ComboBox<String>> boxex;

    @SuppressWarnings("unchecked")
    @FXML private void initialize() {
        boxex = new ArrayList<>();
        root.getChildren().forEach(consumer -> {
            BorderPane pane = (BorderPane) consumer;

            HBox container = (HBox) pane.getChildren().get(0);

            boxex.add((ComboBox<String>) container.getChildren().get(1));
        });
    }

    private void setContext() {

    }

    private void setData() {

    }
}
