package com.fomaster.vgamepad.util;

/**
 * Created by felip_000 on 10-01-2017.
 */
public final class BluetoothAddress {

    private BluetoothAddress() {}

    public static String format(String stringAddress) {

        if (stringAddress == null) return "";

        if (stringAddress.length() != 12) return stringAddress;

        StringBuilder out = new StringBuilder(stringAddress.substring(0, 2));

        for (int i = 2; i < stringAddress.length(); i += 2) {
            out.append(":").append(stringAddress.substring(i, i + 2));
        }

        return out.toString();
    }
}
