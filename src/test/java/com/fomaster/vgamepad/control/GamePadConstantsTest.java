package com.fomaster.vgamepad.control;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by felip_000 on 18-03-2017.
 */
public class GamePadConstantsTest {

    @Test
    public void testIsKeyPressedTrue() {
        Assert.assertTrue(GamePadConstants.isKeyPressed(0x21));
    }

    @Test
    public void testIsKeyPressedFalse() {
        Assert.assertFalse(GamePadConstants.isKeyPressed(0x1));
    }

    @Test
    public void testGetKeyWithOutState() {
        Assert.assertEquals(GamePadConstants.BUTTON_L1, GamePadConstants.getKeyWithOutState(0x29));
    }

    @Test
    public void testGetKeyWithOutStateFalse() {
        Assert.assertEquals(GamePadConstants.BUTTON_L1, GamePadConstants.getKeyWithOutState(GamePadConstants.BUTTON_L1));
    }
}
