package com.fomaster.vgamepad.control;

import com.fomaster.vgamepad.control.util.KeyUtils;
import javafx.scene.input.KeyCode;
import static org.junit.Assert.assertEquals;

import org.junit.Assert;
import org.junit.Test;

import java.awt.event.KeyEvent;

/**
 * Created by felipe on 02-03-17.
 */
public class KeyUtilsTest {

    @Test
    public void testConvertAxis() {
        assertEquals(KeyEvent.VK_RIGHT, KeyUtils.toAWTKeyEvent(KeyCode.RIGHT));
    }

    @Test
    public void testConvertDigit() {
        assertEquals(KeyEvent.VK_5, KeyUtils.toAWTKeyEvent(KeyCode.DIGIT5));
    }

    @Test
    public void testConvertCharacter() {
        assertEquals(KeyEvent.VK_J, KeyUtils.toAWTKeyEvent(KeyCode.J));
    }

    @Test
    public void testConvertFKeys() {
        assertEquals(KeyEvent.VK_F8, KeyUtils.toAWTKeyEvent(KeyCode.F8));
    }

    @Test
    public void testConvertSign() {
        assertEquals(KeyEvent.VK_SLASH, KeyUtils.toAWTKeyEvent(KeyCode.SLASH));
    }

    @Test
    public void testConvertAction() {
        assertEquals(KeyEvent.VK_SHIFT, KeyUtils.toAWTKeyEvent(KeyCode.SHIFT));
    }

    @Test
    public void testConvertFx() {
        Assert.assertEquals(KeyCode.LEFT, KeyUtils.toFxKeyCode(KeyEvent.VK_LEFT));
    }
}
