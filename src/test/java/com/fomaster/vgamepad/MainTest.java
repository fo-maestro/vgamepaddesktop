package com.fomaster.vgamepad;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

/**
 * Created by felipe on 02-03-17.
 */
public class MainTest {
    ArrayList<String> profiles;

    @Before
    public void setUp() {
        profiles = new ArrayList<>();

        for (int i = 0; i < 50; i++) {
            profiles.add("str " + i);
        }
    }

    @Test(timeout = 100)
    public void linearSearchTime() {
        profiles.forEach(System.out::println);
    }
}
