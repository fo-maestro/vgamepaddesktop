package com.fomaster.vgamepad.util;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by felipe on 03-03-17.
 */
public class BluetoothAddressTest {

    @Test
    public void testFormat() {
        Assert.assertEquals(17, BluetoothAddress.format("00B587AS2E4R").length());
    }

    @Test
    public void testFormatNull() {
        Assert.assertNotNull(BluetoothAddress.format(null));
    }

    @Test
    public void testFormatStrEmpty() {
        Assert.assertEquals("", BluetoothAddress.format(""));
    }

    @Test
    public void testFormatStrOdd() {
        Assert.assertEquals("", BluetoothAddress.format("03B5Y"));
    }

    @Test
    public void testFormatStrLessThan12() {
        Assert.assertEquals("", BluetoothAddress.format("00RT98U1"));
    }
}
